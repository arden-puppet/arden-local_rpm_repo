require 'spec_helper'
require 'deep_merge'

describe 'local_rpm_repo' do
  # Ensure compilation works for each supported OS
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }
      let(:params) do
        {
          mirror_fqdn: 'yum.example.com',
          repo_base_path: '/var/yumrepos',
          repo_base_cache_path: '/var/cache/yumrepos',
          base_url_default: 'http://bin.example.com',
        }
      end

      context 'with empty repo list' do
        it 'errors' do
          is_expected.to compile.with_all_deps.and_raise_error(%r{At least one repo must be specified!})
        end
      end

      context 'with vaild repos' do
        before(:each) do
          params.merge!(
            repo_list: {
              'repo1' => {
                'source_type' => 'tar',
                'version' => '1.0.0.0',
              },
              'repo2' => {
                'source_type' => 'puppet',
                'version' => '1.7.1.1',
                'base_url' => 'puppet:///external_files',
              },
              'ibm-xl-compiler-runtime-7' => {
                'source_type' => 'reposync',
                'base_url' => 'ftp://public.dhe.ibm.com/software/server/POWER/Linux/rte/xlcpp/be/rhel7',
                'gpgcheck' => false,
                'array_arch' => ['ppc64'],
              },
            },
          )
        end

        it 'compiles with all dependencies' do
          is_expected.to compile.with_all_deps
        end

        it 'creates the global directories' do
          is_expected.to contain_file(params[:repo_base_path]).with(
            ensure: 'directory',
            owner: 'root',
            group: 'root',
            mode: '0755',
          )
          is_expected.to contain_file(params[:repo_base_cache_path]).with(
            ensure: 'directory',
            owner: 'root',
            group: 'root',
            mode: '0755',
          )
        end

        it 'creates the apache vhost' do
          is_expected.to contain_apache__vhost('yum.example.com').with(
            port: 80,
            docroot: params[:repo_base_path],
          ).that_requires("File[#{params[:repo_base_path]}]")
        end

        it 'creates source instances' do
          params[:repo_list].each do |name, data|
            source_type = data['source_type']
            base_url = if data.key? 'base_url'
                         data['base_url']
                       else
                         params[:base_url_default]
                       end
            is_expected.to contain_createrepo(name).with(
              suppress_cron_stdout: true,
              require: [
                "File[#{params[:repo_base_path]}]",
                "File[#{params[:repo_base_cache_path]}]",
              ],
            )
            is_expected.to contain_file("#{params[:repo_base_path]}/#{name}/versions").with(
              ensure: 'directory',
              owner: 'root',
              group: 'root',
              mode: '0755',
            ).that_requires("Createrepo[#{name}]")
            case source_type
            when %r{^tar}
              is_expected.to contain_local_rpm_repo__tar_source(name).with(
                version: data['version'],
                base_url: base_url,
                versions_path: "#{params[:repo_base_path]}/#{name}/versions",
              ).that_requires("Createrepo[#{name}]")
            when %r{^reposync}
              is_expected.to contain_local_rpm_repo__reposync_source(name).with(
                base_url: base_url,
                gpgcheck: data['gpgcheck'],
                array_arch: data['array_arch'],
                versions_path: "#{params[:repo_base_path]}/#{name}/versions",
              ).that_requires("Createrepo[#{name}]")
            else
              is_expected.to contain_local_rpm_repo__puppet_source(name).with(
                version: data['version'],
                base_url: base_url,
                versions_path: "#{params[:repo_base_path]}/#{name}/versions",
              ).that_requires("Createrepo[#{name}]")
            end
          end
        end
      end
    end
  end
end
