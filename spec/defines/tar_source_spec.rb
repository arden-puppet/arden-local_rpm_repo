require 'spec_helper'
require 'deep_merge'

describe 'local_rpm_repo::tar_source' do
  let(:title) { 'repo1' }
  let(:params) do
    {
      version: '1.0.0.0',
      base_url: 'http://bin.example.com',
      versions_path: "/var/yumrepos/#{title}/versions",
    }
  end

  it 'creates the repo directory' do
    is_expected.to contain_file("create_#{title}").with(
      ensure: 'directory',
      path: "#{params[:versions_path]}/#{params[:version]}",
      owner: 'root',
      group: 'root',
    )
  end

  it 'downloads, extracts, and then removes the repo tar file' do
    version = params[:version]
    versions_path = params[:versions_path]
    is_expected.to contain_exec("wget_#{title}").with(
      command: "/usr/bin/wget #{params[:base_url]}/#{title}-#{version}.tar -O /tmp/#{title}-#{version}.tar",
      refreshonly: true,
    ).that_subscribes_to("File[create_#{title}]")
    is_expected.to contain_exec("extract_#{title}").with(
      command: "/bin/tar -xvf /tmp/#{title}-#{version}.tar",
      cwd: "#{versions_path}/#{version}",
      refreshonly: true,
    ).that_subscribes_to("Exec[wget_#{title}]")
    is_expected.to contain_exec("cleanup_tar_#{title}").with(
      command: "/bin/rm /tmp/#{title}-#{version}.tar",
      refreshonly: true,
    ).that_subscribes_to("Exec[extract_#{title}]")
  end

  it 'overrides source url when prefix is set' do
    version = params[:version]
    params[:prefix] = 'alternate_source'
    is_expected.to contain_exec("wget_#{title}").with(
      command: "/usr/bin/wget #{params[:base_url]}/#{params[:prefix]}-#{version}.tar -O /tmp/#{title}-#{version}.tar",
      refreshonly: true,
    ).that_subscribes_to("File[create_#{title}]")
  end

  it 'fixes ownership on the extracted files and cleans old versions' do
    version = params[:version]
    versions_path = params[:versions_path]
    repo_path = "#{versions_path}/#{version}"
    is_expected.to contain_exec("fix_owner_#{title}").with(
      command: "/bin/chown -R root:root #{repo_path}",
      refreshonly: true,
    ).that_subscribes_to("Exec[extract_#{title}]")
    is_expected.to contain_exec("cleanup_versions_#{title}").with(
      command: "/bin/ls | /bin/grep -v #{version} | /bin/xargs /bin/rm -rf",
      cwd: versions_path,
      refreshonly: true,
    ).that_subscribes_to("Exec[extract_#{title}]")
  end
end
