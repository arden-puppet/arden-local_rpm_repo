require 'spec_helper'
require 'deep_merge'

repo_arguments = {
  'ibm-xl-compiler-runtime-7' => {
    base_url: 'ftp://public.dhe.ibm.com/software/server/POWER/Linux/rte/xlcpp/be/rhel7',
    gpgcheck: true,
    description: 'IBM XL Compiler Runtime',
    initial_timeout: 600,
    array_arch: [
      'ppc64',
      'ppc64le',
    ],
    gpgkey: {
      name: 'RPM-GPG-KEY-ibm-xl-compiler-runtime',
      content: <<~HEREDOC,
        -----BEGIN PGP PUBLIC KEY BLOCK-----
        Version: GnuPG v1.4.5 (GNU/Linux)

        mQGiBE3VkcwRBACTYo/klVrN+Iy5sdyZVf0B9Qm+WgM/Re+tlwdkh3mSnDFLqxCI
        +9knExRvJ1vDYotHEafvHuGSDzee2AKzbLwAqqh+zSM7GMMekWPSskBWg69XC9W9
        hR3v+607pYbzO8QQcSwovm4onq9sKJDLRzC8mRq5UHnJCZgVJZeO+TYIjwCg4VXE
        UfWwdB7jLNJwqSDFrHk1R5cD/jd1O7Maa5RKyiEo8s0HDO8O6TW9FWkr2D5KnkEn
        wXwKrDE+XH/shdOBhC6nEs7ZJTLfHutU60p/ujzrpMIwob6cmMT32wQBYHP5aeZI
        dQZ1FOQjYbqF3p/8vVWrkvenqsBFSLfAuD3ccrh8ylCXUPQIxzI87zdlOmQp2oIo
        VX8IA/4mB8W9lF/6dwQOnWeEK90eEuKmfJSev2TPNQ5qavexw58wEm9dC5BN0aHS
        yLHxhamU4x8ypOi5L5/iAOFEaAn5IuQa8BWR7lZoPH9tGu0iAmSqqf3tR4JUtOln
        qIGbqNYjuc2DHKuZ+bISQxnwEtPXZm0XnIWHwil7o3LyNcoRirRKTGludXggb24g
        UE9XRVIgKElCTSBMaW51eCBUZWNobm9sb2d5IENlbnRlcikgPHBvd2VyeXVtQGxp
        bnV4LnZuZXQuaWJtLmNvbT6IXwQTEQIAIAUCTdWRzAIbAwYLCQgHAwIEFQIIAwQW
        AgMBAh4BAheAAAoJEAooWLA+bkK+GQQAnRou/53e8eI+Zihf/EugfNo5EdzeAJjG
        dAd2NHN6MkYpB27ODVrQVAOUuQINBE3VkfEQCADaFKb7Wf8cAnDX2/jojidHCFE/
        NosKiMRGuUGvGhNyPqIu7P/xL6bnhs8Lpw3kByzb1IL7DI/ooae0BtSHrvp4fDSX
        iU4jlEwpKX1LAzF3FMnWujXCp5JHjj7xeQ53XkYy8FW4fCxWV5wLj9rKYp6XrDmP
        0djxiS2XgrUz8LQE0lEiEE1w/IQK0V2cWhisY7BTDbv9vtFWxMKqxXOljx58b68h
        V3v57v0/YXOaPQBhBRL2Q2wG8QUaDpE+7mlz3TSLaP0CVzOOL/YpDIoj3GoLwYlN
        i7v5wvEVKp+T5I6vEKfiiTNnFTjErVEFo3WkRAg9mANVYe3TlRg0/KUcQHVfAAMH
        CADHOx4/ubpKtPGjjOYW9L1jcOLQXTheOWclJNwhpK2TEdrw3eV1MVmpzMZlQ0Ss
        W7hFUOsamkK1215W13/Q9x1Pqsj3dBpFIygJkxujGXBR1MQiOPx1KDBKw1LLCLtM
        vgk/ZM3HEp54Akbf11Bd9gXBFEsf52cmDE8n31olyCWI0+ZbYyG2HlOax9qJQks9
        piiuBuw472IvHEOWwTNs1iZLYls5Ch1auwWgsy0blD1F5tPUCu8ABknzpUyCNlYC
        Sr8M8J21rQ38BfS3eleO/M8thuzzZWmvs/hyQj3VmDFpX8DDMcrSEdXzHJIedpqF
        e08m1/1uUQ7pEICHYFrLszzHiEkEGBECAAkFAk3VkfECGwwACgkQCihYsD5uQr5z
        HgCfdm1z8NvMMsY2V0ZviImMCLqusGUAoIO4ztoXtNu1dfIALa3fblr/o0Lv
        =+Y93
        -----END PGP PUBLIC KEY BLOCK-----
      HEREDOC
    },
  },
  'ibm-power-tools' => {
    base_url: 'http://public.dhe.ibm.com/software/server/POWER/Linux/yum/OSS/RHEL/7/',
    gpgcheck: false,
    description: 'IBM Power Tools',
    initial_timeout: 235,
    array_arch: [
      'ppc64',
    ],
  },
}

describe 'local_rpm_repo::reposync_source' do
  repo_arguments.each do |name, data|
    context "repo #{name}" do
      let(:title) { name }
      let(:params) do
        data.deep_merge!(
          {
            versions_path: "/var/yumrepos/#{title}/versions",
          },
        )
      end

      it 'creates the repo config ini settings' do
        gpgcheck_value = if params[:gpgcheck]
                           1
                         else
                           0
                         end
        ini_settings = {
          'name' => params[:description],
          'baseurl' => params[:base_url],
          'enabled' => 1,
          'gpgcheck' => gpgcheck_value,
        }
        if params[:gpgcheck]
          ini_settings['gpgkey'] = "file:///#{params[:versions_path]}/#{params[:gpgkey][:name]}"
        end
        ini_file_name = "#{params[:versions_path]}/#{title}.repo"
        before_array = (params[:array_arch].map { |i| ["Exec[initial_sync_#{title}_#{i}]", "Cron[refresh_#{title}_#{i}]"] }).flatten
        ini_settings.each do |k, v|
          is_expected.to contain_ini_setting("#{ini_file_name} [#{title}] #{k}").with(
            ensure: 'present',
            section: title,
            setting: k,
            value: v,
            path: ini_file_name,
            before: before_array,
          )
        end
      end

      if data[:gpgcheck]
        it 'creates the gpgkey file' do
          is_expected.to contain_yum__gpgkey("#{params[:versions_path]}/#{params[:gpgkey][:name]}").with(
            ensure: 'present',
            content: params[:gpgkey][:content],
          )
        end
      end

      it 'executes the initial sync and schedules the daily version' do
        repo_file = "#{params[:versions_path]}/#{title}.repo"
        reposync_cmd_base = if params[:gpgcheck]
                              "/bin/reposync -c #{repo_file} -r #{title} --norepopath -n -g"
                            else
                              "/bin/reposync -c #{repo_file} -r #{title} --norepopath -n"
                            end
        params[:array_arch].each do |a|
          reposync_command = "#{reposync_cmd_base} -a #{a} -p #{params[:versions_path]}/#{a}"
          is_expected.to contain_exec("initial_sync_#{title}_#{a}").with(
            command: reposync_command,
            cwd: params[:versions_path],
            refreshonly: true,
            timeout: params[:initial_timeout],
            subscribe: "Ini_setting[#{repo_file} [#{title}] baseurl]",
          )

          is_expected.to contain_cron("refresh_#{title}_#{a}").with(
            command: "#{reposync_command} >/dev/null",
            user: 'root',
            weekday: '*',
          )
        end
      end
    end
  end
end
