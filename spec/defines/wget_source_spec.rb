require 'spec_helper'
require 'deep_merge'

describe 'local_rpm_repo::wget_source' do
  let(:title) { 'ibm-power-tools' }
  let(:params) do
    {
      base_url: 'https://public.dhe.ibm.com/software/server/POWER/Linux/yum/OSS/RHEL/7/',
      versions_path: "/var/yumrepos/#{title}/versions",
      cut_dir_count: 7,
      initial_timeout: 600,
    }
  end

  it 'executes the initial sync and schedules the daily version' do
    wget_command = "/usr/bin/wget -A '*.rpm' --no-parent -c -nH -r --cut-dirs #{params[:cut_dir_count]} -P '#{params[:versions_path]}' '#{params[:base_url]}'"
    is_expected.to contain_exec("initial_sync_#{title}").with(
      command: wget_command,
      cwd: params[:versions_path],
      refreshonly: true,
      timeout: params[:initial_timeout],
    )

    is_expected.to contain_cron("refresh_#{title}").with(
      command: "#{wget_command} >/dev/null",
      user: 'root',
      weekday: '*',
    )
  end
end
