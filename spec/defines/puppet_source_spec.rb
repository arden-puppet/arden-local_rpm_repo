require 'spec_helper'
require 'deep_merge'

describe 'local_rpm_repo::puppet_source' do
  let(:title) { 'repo2' }
  let(:params) do
    {
      version: '1.7.1.1',
      base_url: 'puppet:///external_files',
      versions_path: "/var/yumrepos/#{title}/versions",
    }
  end

  it 'creates the repo directory' do
    is_expected.to contain_file("create_#{title}_#{params[:version]}").with(
      ensure: 'directory',
      path: "#{params[:versions_path]}/#{params[:version]}",
      owner: 'root',
      group: 'root',
      recurse: 'remote',
      source: "#{params[:base_url]}/#{title}/#{params[:version]}",
    )
  end

  it 'fixes ownership on the extracted files and cleans old versions' do
    version = params[:version]
    versions_path = params[:versions_path]
    is_expected.to contain_exec("cleanup_versions_#{title}").with(
      command: "/bin/ls | /bin/grep -v #{version} | /bin/xargs /bin/rm -rf",
      cwd: versions_path,
      refreshonly: true,
    ).that_subscribes_to("File[create_#{title}_#{version}]")
  end
end
