# Changelog

All notable changes to this project will be documented in this file.

## 1.1.0 (2022-07-29)

### Feature (1 change)

- [feat: check and remove files which fail gpg](arden-puppet/arden-local_rpm_repo@76719c296be1e16badacf78d42ad79c5baf0aacd) ([merge request](arden-puppet/arden-local_rpm_repo!4))

## 1.0.0 (2022-07-29)

### Feature (4 changes)

- [feat: configurable first sync timeouts](arden-puppet/arden-local_rpm_repo@191fda9be7d1612cec595be5a0d0bb08b1c0234b) ([merge request](arden-puppet/arden-local_rpm_repo!3))
- [feat: only sync the newest version](arden-puppet/arden-local_rpm_repo@01aefbff52d531763ff6c68eac31e652e98794d0) ([merge request](arden-puppet/arden-local_rpm_repo!3))
- [feat: wget based repository sources](arden-puppet/arden-local_rpm_repo@fd333196f3444e6597ed6a8f4c38754a2546401e) ([merge request](arden-puppet/arden-local_rpm_repo!3))
- [feat: reposync based repository mirror source](arden-puppet/arden-local_rpm_repo@f1969df61315cf83a628840625af5951649ca00a) ([merge request](arden-puppet/arden-local_rpm_repo!3))

### Maintenance (3 changes)

- [maint: add changelog generation instructions](arden-puppet/arden-local_rpm_repo@a78bdce03cdbb8d98abd2b22d82894fbfb2d4341) ([merge request](arden-puppet/arden-local_rpm_repo!3))
- [maint: convert to struct](arden-puppet/arden-local_rpm_repo@a06974539216f8822cbee02b06427894654e91d6) ([merge request](arden-puppet/arden-local_rpm_repo!3))
- [maint: pdk update](arden-puppet/arden-local_rpm_repo@c9cc96ed18e3f07dd16b08664e3a2116c8a1af26) ([merge request](arden-puppet/arden-local_rpm_repo!3))

## Release 0.2.1

[Full Changelog](https://gitlab.com/arden-puppet/arden-local_rpm_repo/compare/0.2.0...0.2.1#)

**Features**
* Allow cron output to be suppressed
* Updated to PDK 1.13.0

## Release 0.2.0

[Full Changelog](https://gitlab.com/arden-puppet/arden-local_rpm_repo/compare/0.1.2...0.2.0#)

**Features**
* Using types for parameters.
* PDK 1.9.1
* Support for puppet custom mount points (file servers)

**Bugfixes**
* Switched to a refresh-only topology - nolonger has idempotent executions of commands

## Release 0.1.2

[Full Changelog](https://gitlab.com/arden-puppet/arden-local_rpm_repo/compare/0.1.1...0.1.2#)

**Bugfixes**
- Added checks to prevent the source files from being downloaded even if the repo is already deployed. Note that it doesn't validate the contents of the repo so if it gets corrupted you'll need to delete.

## Release 0.1.1

[Full Changelog](https://gitlab.com/arden-puppet/arden-local_rpm_repo/compare/0.1.0...0.1.1#)

**Bugfixes**
- Used a series of commands instead of tidy to cleanup old versions as the initial release would simply alternate between removing everything and creating everything.

## Release 0.1.0

**Features**
- Initial release!

