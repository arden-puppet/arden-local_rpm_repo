# Release Process

Someday this should be in CI/CD.

1. Ensure you're a maintainer & have an authentication token.
1. Update metadata and ensure all relevant commits have the appropriate `Changelog: tag` suffix as the last line of the commit.
1. Do the compare & trigger the generation which will create the changelog file itself.

    ```bash
    ID="27203686"
    TOKEN="my-secret-token"

    VERSION="0.1.0"
    FROM="some-commit-id-tag-or-branch"
    curl --header "PRIVATE-TOKEN: ${TOKEN}" \
      --data "version=${VERSION}&from=${FROM}" \
             "https://gitlab.com/api/v4/projects/${ID}/repository/changelog"
    ```

# See also

* [Personal access tokens](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)
