# @summary GPG Key file definition
#
# @param name
#   Filename of the resulting keyfile
#
# @param content
#   Raw content of the keyfile
type Local_rpm_repo::RepoGPGKey = Struct[
  name    => String,
  content => String,
]
