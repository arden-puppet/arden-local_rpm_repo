# @summary URL types for tar source data.
type Local_rpm_repo::SourceUrl = Variant[
  Stdlib::Httpurl,
  Stdlib::Httpsurl,
]
