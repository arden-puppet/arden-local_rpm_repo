# @summary Valid source URL types for reposync
type Local_rpm_repo::RepoUrl = Variant[
  Stdlib::Httpurl,
  Stdlib::Httpsurl,
  Pattern[/(?i:^ftp?:\/\/)/],
]
