# @summary Matches puppet file server source url strings
type Local_rpm_repo::PuppetUrl = Pattern[/(?i:^puppet?:\/\/)/]
