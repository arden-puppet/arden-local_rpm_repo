# @summary Allowed version semantics on our data.
type Local_rpm_repo::Version = Pattern[/\d[.]\d[.]\d[.]\d/]
