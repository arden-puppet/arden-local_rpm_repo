# @summary Repo configuration file used by reposync to download the data
#
# @param source_type
#   Determines the type of source from which the repo content will be retrieved.
#
# @param version
#   Puppet and tar type repositories use these version numbers in their
#   deployment model. 
#
# @param baseurl
#   Source from which the repo content will be downloaded.
#
# @param enabled
#   See man yum.conf
#
# @param gpgcheck
#   See man yum.conf
#
# @param array_arch
#   Reposync based repositories can be multi-architecture
#
# @param description
#   Human readable string used in the declaration of the repository.
#
# @param initial_timeout
#   Controls the command timeout via exec when the sync runs the first time. Not
#   all modes support this parameter. Note that it defaults to 300 seconds when
#   left out.
type Local_rpm_repo::RepoData = Variant[
  Struct[
    source_type        => Enum['puppet'],
    version            => Local_rpm_repo::Version,
    Optional[prefix]   => String,
    Optional[base_url] => Local_rpm_repo::PuppetURL,
  ],
  Struct[
    source_type        => Enum['tar'],
    version            => Local_rpm_repo::Version,
    Optional[prefix]   => String,
    Optional[base_url] => Local_rpm_repo::SourceUrl,
  ],
  Struct[
    source_type               => Enum['reposync'],
    base_url                  => Local_rpm_repo::RepoUrl,
    gpgcheck                  => Boolean,
    array_arch                => Array[String],
    Optional[gpgkey]          => Local_rpm_repo::RepoGPGKey,
    Optional[description]     => String,
    Optional[initial_timeout] => Integer,
  ],
  Struct[
    source_type               => Enum['wget'],
    base_url                  => Local_rpm_repo::RepoUrl,
    cut_dir_count             => Integer,
    Optional[initial_timeout] => Integer,
  ],
]
