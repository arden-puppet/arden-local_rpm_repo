# This type is an implementation detail and should not be declared directly!
#
# @summary Repo with rpm files sourced via a reposync call.
#
# @param base_url
#   Source against which the reposync command will execute
#
# @param versions_path
#   Absolute path to the versions subdirectory for this repository. This will be
#   used as the target for the wget command.
#
# @param cut_dir_count
#   When wget recursively downloads the contents of a webpage the default
#   behavior is to create a folder for each subdirectory along the path. This
#   parameter controls the number of directories to cut making the resulting
#   tree more concise.
#
# @param initial_timeout
#   Execution timeout on the first synchronization of the repository.
#
define local_rpm_repo::wget_source (
  Local_rpm_repo::RepoUrl $base_url,
  Stdlib::AbsolutePath $versions_path,
  Integer $cut_dir_count,
  Integer $initial_timeout            = 300,
) {
  $wget_command_base = "/usr/bin/wget -A '*.rpm' --no-parent -c -nH -r"
  $wget_arguments = "--cut-dirs ${cut_dir_count} -P '${versions_path}' '${base_url}'"
  $wget_command = "${wget_command_base} ${wget_arguments}"
  exec { "initial_sync_${name}":
    command     => $wget_command,
    cwd         => $versions_path,
    refreshonly => true,
    timeout     => $initial_timeout,
  }
  cron { "refresh_${name}":
    command => "${wget_command} >/dev/null",
    user    => 'root',
    hour    => fqdn_rand(23, "hour_${name}"),
    minute  => fqdn_rand(59, "minute_${name}"),
    weekday => '*',
  }
}
