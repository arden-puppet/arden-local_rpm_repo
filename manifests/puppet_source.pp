# This type is an implementation detail and should not be declared directly!
#
# @summary Repo with rpm files sourced from a puppet file server.
#
# @param version
#   Version number to instantiate.
#
# @param base_url
#   Puppet path to the RPM directory. See the repo::base_url_default for
#   further details.
#
# @param prefix
#   String prepended to the version number to determine the source tar file
#   name. Note that this field is a namevar.
#
# @param versions_path
#   Absolute path to the versions subdirectory for this repository.
#
define local_rpm_repo::puppet_source (
  Local_rpm_repo::Version $version,
  Local_rpm_repo::PuppetUrl $base_url,
  Stdlib::AbsolutePath $versions_path,
  String $prefix                      = $name,
) {
  # Create a subdirectory for the new version 
  $repo_rpm_path = "${versions_path}/${version}"
  file { "create_${name}_${version}":
    ensure  => 'directory',
    path    => $repo_rpm_path,
    owner   => 'root',
    group   => 'root',
    recurse => 'remote',
    source  => "${base_url}/${prefix}/${version}",
  }

  # Clean out any old versions
  $cleanup_command = "/bin/ls | /bin/grep -v ${version} | /bin/xargs /bin/rm -rf"
  exec { "cleanup_versions_${name}":
    command     => $cleanup_command,
    cwd         => $versions_path,
    refreshonly => true,
    subscribe   => File["create_${name}_${version}"],
  }
}
