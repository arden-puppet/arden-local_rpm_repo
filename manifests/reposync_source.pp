# This type is an implementation detail and should not be declared directly!
#
# @summary Repo with rpm files sourced via a reposync call.
#
# @param base_url
#   Source against which the reposync command will execute
#
# @param versions_path
#   Absolute path to the versions subdirectory for this repository. This will be
#   used as the target for the reposync command.
#
# @param gpgcheck
#   When true, this module will attempt to initialize the gpgkey file & will
#   ensure it is valid during this configuration
#
# @param gpgkey
#   This parameter must be included when gpgcheck is enabled. Includes the
#   partial name of the keyfile as well as it's desired content.
#
# @param array_arch
#   List of processor architectures to iterate through.
#
# @param description
#   When present, the config file can have a different "name" subkey in its INI.
#
# @param initial_timeout
#   Execution timeout on the first synchronization of the repository.
#
define local_rpm_repo::reposync_source (
  Local_rpm_repo::RepoUrl $base_url,
  Stdlib::AbsolutePath $versions_path,
  Boolean $gpgcheck,
  String $description                          = $name,
  Integer $initial_timeout                     = 300,
  Array[String] $array_arch                    = [],
  Optional[Local_rpm_repo::RepoGPGKey] $gpgkey = undef,
) {
  # Determine whether the gpg key settings are vaild
  $before_initial_sync_tmp = $array_arch.map |$arch| {
    [
      "Exec[initial_sync_${name}_${arch}]",
      "Cron[refresh_${name}_${arch}]",
    ]
  }
  $before_initial_sync = flatten($before_initial_sync_tmp)
  if $gpgcheck {
    # Fail if we have this enabled and the key data is not declared
    unless($gpgkey != undef) {
      fail("Repo ${name}: gpgkey must be provided if gpgcheck is enabled!")
    }

    # Configure the key file
    $gpgkey_path = "${versions_path}/${gpgkey['name']}"
    yum::gpgkey { $gpgkey_path:
      ensure  => 'present',
      content => $gpgkey['content'],
      before  => $before_initial_sync,
    }

    $ini_gpg = {
      'gpgcheck' => 1,
      'gpgkey'   => "file:///${gpgkey_path}",
    }

    $require_gpg = [
      "Yum::gpgkey['${gpgkey_path}']",
    ]
    $reposync_command_gpg = ' -g'
  } else {
    $ini_gpg = {
      'gpgcheck' => 0,
    }
    $reposync_command_gpg = ''
  }

  # Initialize the repo config file
  $repo_file = "${versions_path}/${name}.repo"
  $ini_defaults = {
    'path'   => $repo_file,
    'before' => $before_initial_sync,
  }
  $ini_settings = {
    $name        => {
      'name'     => $description,
      'baseurl'  => $base_url,
      'enabled'  => 1,
    } + $ini_gpg,
  }
  inifile::create_ini_settings($ini_settings, $ini_defaults)

  if(length($array_arch) == 0) {
    fail("Repo ${name}: at least one architecture must be specified!")
  }
  $reposync_command_base = "/bin/reposync -c ${repo_file} -r ${name} --norepopath -n${reposync_command_gpg}"
  $array_arch.each |$arch| {
    $reposync_command = "${reposync_command_base} -a ${arch} -p ${versions_path}/${arch}"
    exec { "initial_sync_${name}_${arch}":
      command     => $reposync_command,
      cwd         => $versions_path,
      refreshonly => true,
      timeout     => $initial_timeout,
      subscribe   => Ini_setting["${repo_file} [${name}] baseurl"],
    }
    cron { "refresh_${name}_${arch}":
      command => "${reposync_command} >/dev/null",
      user    => 'root',
      hour    => fqdn_rand(23, "hour_${name}_${arch}"),
      minute  => fqdn_rand(59, "minute_${name}_${arch}"),
      weekday => '*',
    }
  }
}
