# This type is an implementation detail and should not be declared directly!
#
# @summary Repo with instance data retrieve from a tar file.
#
# @param version
#   Version number to instantiate.
#
# @param base_url
#   Location containing the source tar file. See the repo::base_url_default for
#   further details.
#
# @param prefix
#   String prepended to the version number to determine the source tar file
#   name. Note that this field is a namevar.
#
# @param versions_path
#   Absolute path to the versions subdirectory for this repository.
#
define local_rpm_repo::tar_source (
  Local_rpm_repo::Version $version,
  Variant[Stdlib::Httpurl, Stdlib::Httpsurl] $base_url,
  Stdlib::AbsolutePath $versions_path,
  String $prefix                                       = $name,
) {
  # Create a subdirectory for the new version 
  $repo_rpm_path = "${versions_path}/${version}"
  file { "create_${name}":
    ensure => 'directory',
    path   => $repo_rpm_path,
    owner  => 'root',
    group  => 'root',
  }

  # Retrieve the latest fileset if necessary
  $source_file = "${base_url}/${prefix}-${version}.tar"
  $dest_file = "/tmp/${name}-${version}.tar"
  $retrieve_command = "/usr/bin/wget ${source_file} -O ${dest_file}"
  exec { "wget_${name}":
    command     => $retrieve_command,
    refreshonly => true,
    subscribe   => File["create_${name}"],
  }

  # Deploy the fileset to the target directory
  exec { "extract_${name}":
    command     => "/bin/tar -xvf ${dest_file}",
    cwd         => $repo_rpm_path,
    refreshonly => true,
    subscribe   => Exec["wget_${name}"],
  }

  # Delete the temporary tar file
  exec { "cleanup_tar_${name}":
    command     => "/bin/rm ${dest_file}",
    refreshonly => true,
    subscribe   => Exec["extract_${name}"],
  }

  # Correct the ownership on all the resulting files
  exec { "fix_owner_${name}":
    command     => "/bin/chown -R root:root ${repo_rpm_path}",
    refreshonly => true,
    subscribe   => Exec["extract_${name}"],
  }

  # Clean out any old versions
  $cleanup_command = "/bin/ls | /bin/grep -v ${version} | /bin/xargs /bin/rm -rf"
  exec { "cleanup_versions_${name}":
    command     => $cleanup_command,
    cwd         => $versions_path,
    refreshonly => true,
    subscribe   => Exec["extract_${name}"],
  }
}
