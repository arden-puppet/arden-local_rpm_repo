# local_rpm_repo

[![pipeline status](https://gitlab.com/arden-puppet/arden-local_rpm_repo/badges/master/pipeline.svg)](https://gitlab.com/arden-puppet/arden-local_rpm_repo/commits/master)
[![Version](https://img.shields.io/puppetforge/v/arden/local_rpm_repo.svg)](https://forge.puppet.com/arden/local_rpm_repo)

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with local_rpm_repo](#setup)
    * [What local_rpm_repo affects](#what-local_rpm_repo-affects)
    * [Setup requirements](#setup-requirements)
3. [Usage - Configuration options and additional functionality](#usage)
4. [Limitations - OS compatibility, etc.](#limitations)
5. [Development - Guide for contributing to the module](#development)

## Description

This module allows you to create and deploy a http based RPM repository using a tar of RPMs as a source or an external puppet fileserver. This can be helpful for environments where some number of locally created and maintained RPMs must be distributed to a set of systems.

We use this to automatically maintain a simple repo containing a ppc64 build of puppet-agent.

## Setup

### What local_rpm_repo affects

It creates an apache vhost on the target machine and schedules a cron job of `createrepo` for each repository specified in the config. The repos will be within the specified vhost in their own folders.

### Setup Requirements - Tar Source

When an http or https URL is provided the module assumes you have a simple web server configured to host a tar file for each repository you intend to deploy.

Tar files for each repository should be created and named with a format similar to `${reponame}-${version}.tar`. For example, a repo named *test-repo* with a version string of *1.6.3.0* would have a corresponding tar named `test-repo-1.6.3.0.tar`. It is possible to override the `${reponame}` portion of this string with an alternate prefix. See the reference documentation for detail.

### Setup Requirements - Puppet Source

Configure a [custom file server mount point](https://puppet.com/docs/puppet/5.5/file_serving.html) to host the source data for your repositories. The `base_url` should contain a directory corresponding to the repository name or the prefix override configured for that repository. Within that directory there should be a folder for the target version which contains the desired RPMs in the expected deployment structure.

## Usage

### Tar Sources

The example below will configure three repos named repo1, repo2, and repo3 respectively on the target node. This node will expect connections to arrive to the vhost `yum.example.com`.

When puppet is applying this configuration it will expect to be able to download the following three files:
* `http://bin.example.com/data/repo1-5.0.3.1.tar`
* `http://bin.example.com/data/repo2-1.0.7.0.tar`
* `http://bin.example.com/data/repo3-dev-1.0.0.0.tar`

```puppet
class local_rpm_repo {
  base_url_default => 'http://bin.example.com/data/',
  mirror_fqdn      => 'yum.example.com',
  repo_list        => {
    'repo1' => {
      'source_type' => 'tar',
      'version' => '5.0.3.1',
    },
    'repo2' => {
      'source_type' => 'tar',
      'version' => '1.0.7.0',
    },
    'repo3' => {
      'source_type' => 'tar',
      'version' => '5.0.3.1',
      'prefix'  => 'repo3-dev',
    },
  },
}
```

Alternatively the same configuration can be achieved via hiera:

```yaml
---
local_rpm_repo::base_url_default: 'http://bin.example.com/data/'
local_rpm_repo::mirror_fqdn: 'yum.example.com'
local_rpm_repo::repo_list:
  repo1:
    source_type: 'tar'
    version: '5.0.3.1'
  repo2:
    source_type: 'tar'
    version: '1.0.7.0'
  repo3:
    source_type: 'tar'
    prefix: 'repo3-dev'
    version: '1.0.0.0'
```

Once applied, the following changes will be implemented:

The `/var/yumrepos` directory will contain the following sub folders:
* `/var/yumrepos/repo1/versions/5.0.3.1/`
* `/var/yumrepos/repo2/versions/1.0.7.0/`
* `/var/yumrepos/repo3/versions/1.0.0.0/`

The contents of the three tar files detailed above will be extracted to these directories.

An Apache vhost will be created listening at `yum.example.com` serving the `/var/yumrepos` directory.

### Puppet Fileserver

This method uses the `source` directive on the [puppet file resource type](https://puppet.com/docs/puppet/5.5/types/file.html) to recursively replicate the contents of the specified source folder as the repo version.

```yaml
---
local_rpm_repo::base_url_default: 'http://bin.example.com/data/'
local_rpm_repo::mirror_fqdn: 'yum.example.com'
local_rpm_repo::repo_list:
  repo1:
    source_type: 'tar'
    version: '5.0.3.1'
  repo2:
    source_type: 'puppet'
    base_url: 'puppet:///external_files'
    version: '1.0.7.0'
```

In the example above repo1 will be declared from the tar `http://bin.example.com/data/repo1-5.0.3.1.tar` as normal, however, repo2 will replicate the contents of `puppet:///external_files/repo2/1.0.7.0` to it's version directory.

### Reposync

This method schedules a daily execution of [reposync](https://linux.die.net/man/1/reposync) via cron to pull down the latest package files from an external repository. A separate job will be created for each CPU architecture and stored within the version folder used by other sync methods.

```yaml
---
local_rpm_repo::base_url_default: 'http://bin.example.com/data/'
local_rpm_repo::mirror_fqdn: 'yum.example.com'
local_rpm_repo::repo_list:
  repo1:
    source_type: 'tar'
    version: '5.0.3.1'
  ibm-xl-compiler-runtime-7:
    source_type: 'reposync'
    base_url: 'ftp://public.dhe.ibm.com/software/server/POWER/Linux/rte/xlcpp/be/rhel7'
    gpgcheck: false,
    array_arch:
      - 'ppc64'
      - 'ppc64le'
```

Note that gpg configuration is supported in the context of this repository using the original key files from the remote repository.

Currently only publicly reachable repositories can be configured.

### Wget

This method uses wget to recursively download the `*.rpm` files from a particular URL. It is intended to serve as a backup for repositories which have bad meta-data but good packages (Looking at you IBM).

```yaml
---
local_rpm_repo::base_url_default: 'http://bin.example.com/data/'
local_rpm_repo::mirror_fqdn: 'yum.example.com'
local_rpm_repo::repo_list:
  repo1:
    source_type: 'tar'
    version: '5.0.3.1'
  ibm-power-tools:
    source_type: 'wget'
    base_url: 'https://public.dhe.ibm.com/software/server/POWER/Linux/yum/OSS/RHEL/7/'
    cut_dir_count: 7
```

## Limitations

* Currently GPG signing of custom repositories is not supported.

## Development

TBD.

## Contributors

Check out the [contributor list](https://gitlab.com/arden-puppet/arden-local_rpm_repo/graphs/master).
